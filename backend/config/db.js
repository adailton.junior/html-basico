const sequelize = require('sequelize');

const db = new sequelize({
    dialect: 'sqlite',
    storage: './database/mydatabase.db'
});

module.exports = db;
