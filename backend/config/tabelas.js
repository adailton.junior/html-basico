
class Tabelas {
    init(conexao) {
        this.conexao = conexao;
        this.criarPessoas();
    }

    criarPessoas() {
        const sql = 'CREATE TABLE IF NOT EXISTS pessoas (id INTEGER PRIMARY KEY, nome varchar(50) NOT NULL, nomeMae varchar(50), nomePai varchar(50), cpf varchar(11) NOT NULL)';

        this.conexao.serialize(() => {
            this.conexao.run(sql);
        });
    }
}

module.exports = new Tabelas;
