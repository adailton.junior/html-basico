var urlBase = 'https://jogo-api.adailtonjunior2.repl.co'

function aoCarregarLista(){
    console.log("Função para reiniciar o estado da tela no carregamento...")
    document.getElementById('btProximo').disabled = true;
    document.getElementById('btAnterior').disabled = true;
    document.getElementById('categoria').value = '';

    let tabela = document.getElementById('tabJogos');
    let totalJogo = document.getElementById('totalJogo');
    limparTabela(tabela, totalJogo);
}

function limparTabela(tabela, totalJogo) {
    let linhas = tabela.rows;
    let i = linhas.length;
    totalJogo.value = 0;
    while (--i) {
        tabela.deleteRow(i);
    }
}

function buscar(tabela, totalJogo, categoria, paginaAtual) {
    limparTabela(tabela, totalJogo);

    let url = `${urlBase}/jogos?pagina=${paginaAtual.value}`;
    if(categoria.value !== '') {
        url += `&categoria=${categoria.value}`;
    }

    fetch(url).then(
        (response) => {
            response.json().then((data) => {
                totalJogo.value = data.total;
                data.jogos.forEach(jogo => {
                    let linha = tabela.insertRow(-1);
                    linha.insertCell(0).innerHTML = jogo.id;
                    linha.insertCell(1).innerHTML = jogo.nome;
                    linha.insertCell(2).innerHTML = jogo.categoria;
                    linha.insertCell(3).innerHTML = jogo.ano;
                    linha.insertCell(4).innerHTML = `<button onclick="visualizar(${jogo.id})">Visualizar</button>`;
                });
            })
        }
    );
}

function pesquisar() {
    let tabela = document.getElementById('tabJogos');
    let totalJogo = document.getElementById('totalJogo');
    let categoria = document.getElementById('categoria');
    let paginaAtual = document.getElementById('paginaAtual');
    
    paginaAtual.value = 0;

    // Habilitando o botão de próximo
    document.getElementById('btProximo').disabled = false;
    buscar(tabela, totalJogo, categoria, paginaAtual);
}

function anterior() {
    let tabela = document.getElementById('tabJogos');
    let totalJogo = document.getElementById('totalJogo');
    let categoria = document.getElementById('categoria');
    let paginaAtual = document.getElementById('paginaAtual');

    // Habilitando o botão de próximo
    document.getElementById('btProximo').disabled = false;
    
    paginaAtual.value = parseInt(paginaAtual.value) - 1;
    //Verificando se o botão anterior deve ser desabilitado
    if (parseInt(paginaAtual.value) === 0) {
        document.getElementById('btAnterior').disabled = true;
    }

    buscar(tabela, totalJogo, categoria, paginaAtual);
}

function proximo() {
    let tabela = document.getElementById('tabJogos');
    let totalJogo = document.getElementById('totalJogo');
    let categoria = document.getElementById('categoria');
    let paginaAtual = document.getElementById('paginaAtual');

    //Habilitando o botão de anterior
    document.getElementById('btAnterior').disabled = false;

    let qntPagina = Math.ceil(parseInt(totalJogo.value)/5);
    paginaAtual.value = parseInt(paginaAtual.value) + 1;

    //Verificando se o botão próximo deve ser desabilitado
    if (parseInt(paginaAtual.value) + 1 === qntPagina) {
        document.getElementById('btProximo').disabled = true;
    }

    buscar(tabela, totalJogo, categoria, paginaAtual);
}

function cadastrar() {
    let nome = document.getElementById('nome');
    let categoria = document.getElementById('categoria');
    let ano = document.getElementById('ano');
    let descricao = document.getElementById('descricao');
    
    if (nome.value === '' || categoria.value === '' || ano.value === '') {
        alert('Existem campos obrigatórios!');
        return;
    }

    fetch(`${urlBase}/jogos`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            nome: nome.value,
            categoria: categoria.value,
            ano: ano.value,
            descricao: descricao.value
        })
    }).then((response) => {
        response.json().then((data) => {
            if (data.status === "success") {
                alert(`Jogo ${nome.value} cadastrado com sucesso`);
                nome.value = '';
                categoria.value = '';
                ano.value = '';
            } else if (data.status === "error") {
                alert("ERROR: " + data.message);
            } else {
                alert("Status desconhecido!");
            }
        })
    });
}

function visualizar(idJogo) {
    window.location = 'visualizar.html?id=' + idJogo;
}

function aoCarregarLista() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id');
    document.getElementById('id').value = id;
    
    fetch(`${urlBase}/jogos/${id}`).then(
        (response) => {
            response.json().then((data) => {
                console.log(data)
                if (data.status === "success") {
                    document.getElementById('nome').value = data.jogo.nome;
                    document.getElementById('categoria').value = data.jogo.categoria;
                    document.getElementById('ano').value = data.jogo.ano;
                    document.getElementById('descricao').value = data.jogo.descricao;

                    let date = new Date(data.jogo.createdAt);
                    document.getElementById('createdAt').value = date.toLocaleDateString("pt-BR");
                }
            })
        }
    );
}