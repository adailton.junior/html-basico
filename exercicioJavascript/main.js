// Exercicio Math
function calcularEsfera() {
    let raio = parseFloat(document.getElementById('raio').value);
    console.log(raio);

    if (!isNaN(raio)) {
        let area = 4.0 * Math.PI * Math.pow(raio, 2);
        let volume = (4.0 / 3.0) * Math.PI * Math.pow(raio, 3);

        document.getElementById('area').value = area;
        document.getElementById('volume').value = volume;
    } else {
        alert('Raio não é um número válido!');
        document.getElementById('area').value = '';
        document.getElementById('volume').value = '';
    }
}

// Exercicio String
function upperCase() {
    let nome = document.getElementById('nome').value;
    document.getElementById('nome').value = nome.toUpperCase().trim();

    let email = document.getElementById('email').value;
    document.getElementById('email').value = email.toUpperCase().trim();
}

function verificarEmail() {
    let email = document.getElementById('email').value;
    let possui = email.includes('@');
    if (!possui) {
        alert('O email deve possuir @');
    }
}

// Exercicio Date
function verificarDtNascimento() {
    let strDtNascimento = document.getElementById('dtNascimento').value;
    validarData(strDtNascimento);
}

function calcularIdade() {
    let strDtNascimento = document.getElementById('dtNascimento').value;
    if(validarData(strDtNascimento)) {
        let arrDtNascimento = strDtNascimento.split('/');
        let diaNascimento = parseInt(arrDtNascimento[0]);
        let mesNascimento = parseInt(arrDtNascimento[1]);
        let anoNascimento = parseInt(arrDtNascimento[2]);

        let dataAtual = new Date();
        let anoAtual = dataAtual.getFullYear();
        let mesAtual = dataAtual.getMonth() + 1;
        let diaAtual = dataAtual.getDate();

        let idade = anoAtual - anoNascimento;

        if (mesAtual < mesNascimento || (mesAtual == mesNascimento && diaAtual < diaNascimento)) {
            idade--;
        }

        document.getElementById('idade').value = idade;
    }
}

function validarData(str) {
    let arrDtNascimento = str.split('/');

    if (arrDtNascimento.length != 3) {
        errorDate();
        return false;
    }

    let day = parseInt(arrDtNascimento[0]);
    let month = parseInt(arrDtNascimento[1]);
    let year = parseInt(arrDtNascimento[2]);

    // Verificando se nenhuma data é NaN
    if (isNaN(day) || isNaN(month) || isNaN(year)) {
        errorDate();
        return false;
    }

    return true;
}

function errorDate() {
    alert('Data invalida!');
    document.getElementById('dtNascimento').value = '';
}